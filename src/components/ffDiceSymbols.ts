// ---------------------------------------------------------------------------------------------------------------------
// Fantasy Flight Dice Components
// ---------------------------------------------------------------------------------------------------------------------

// Utils
import { titleCase } from '../utils/string';

// Styles
import eoteDiceStyleText from '../styles/eoteDice.scss';
import genDiceStyleText from '../styles/genesysDice.scss';

// ---------------------------------------------------------------------------------------------------------------------

const eoteDiceStyle = document.createElement('style');
eoteDiceStyle.appendChild(document.createTextNode(eoteDiceStyleText));

const genDiceStyle = document.createElement('style');
genDiceStyle.appendChild(document.createTextNode(genDiceStyleText));

// ---------------------------------------------------------------------------------------------------------------------

export class FFDiceSymbol extends HTMLElement
{
    #elem = document.createElement('span');

    constructor()
    {
        super()
        this.attachShadow({ mode: 'open' });

        // Add our element to the root
        this.shadowRoot.appendChild(this.#elem);
    }

    connectedCallback()
    {
        // Set class name
        this.#elem.classList.add(`ff-${ this.getAttribute('symbol') }`);

        // Determine which style to load
        if(this.getAttribute('system') === 'eote')
        {
            // Add the Eote Dice Symbol Style
            this.shadowRoot.appendChild(eoteDiceStyle.cloneNode(true));
        }
        else
        {
            // Add the Genesys Dice Symbol Style
            this.shadowRoot.appendChild(genDiceStyle.cloneNode(true));
        }

        this.setAttribute('title', titleCase(this.getAttribute('symbol')));
    }
}

export function buildFFDiceSymbolWrapper(symbol : string, system ?: 'eote') : CustomElementConstructor
{
    class FFDiceSymbolWrapper extends FFDiceSymbol
    {
        constructor()
        {
            super();
        }

        connectedCallback()
        {
            // Remove any sneaky attempts to override the symbols
            this.removeAttribute('symbol');
            this.removeAttribute('system');

            // Set the symbol to the correct one for this element
            this.setAttribute('symbol', symbol);

            // Set the system, if needed
            if(system === 'eote' || [ 'lightside', 'darkside', 'forcepoint' ].includes(symbol))
            {
                this.setAttribute('system', 'eote');
            }

            super.connectedCallback();
        }
    }

    return FFDiceSymbolWrapper;
}

// ---------------------------------------------------------------------------------------------------------------------
