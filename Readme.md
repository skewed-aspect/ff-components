# FF Web Components

Custom webcomponents for various FF related things, like dice symbols or character stat blocks.

## Usage

These components are designed to be used as easily as possible. If you find any issues using these, please report them.

### Import into Project

Should be as simple as grabbing `ff-components.css`, and `ff-components.es.js` or `ff-components.umd.js` as you need. 
Include those in your project, and it should be good to go.

_Not going to lie, right now I'm a bit iffy on this, till I start using it. I'll get this updated once I understand how 
this all works a bit better._

### FF Dice Symbols

There are two styles of components supported. First, there's the low level `ff-symbol` component:

```html

<!-- Ability Dice Symbol -->
<ff-symbol symbol="ability"></ff-symbol>

<!-- Genesys Success Symbol -->
<ff-symbol symbol="success" system="genesys"></ff-symbol>

<!-- EotE Success Symbol -->
<ff-symbol symbol="success" system="eote"></ff-symbol>
```

This component is very verbose by design. It's simple to use, it's API is like falliung off a log, but it's a touch 
annoying to write out all the time.

That's why we have the shortcut symbols:

```html
<!-- Edge of the Empire Symbols -->
<ff-success-eote></ff-success-eote>
<ff-advantage-eote></ff-advantage-eote>
<ff-triumph-eote></ff-triumph-eote>
<ff-lightside></ff-lightside>
<ff-forcepoint></ff-forcepoint>
<ff-failure-eote></ff-failure-eote>
<ff-threat-eote></ff-threat-eote>
<ff-despair-eote></ff-despair-eote>
<ff-darkside-eote></ff-darkside-eote>

<!-- Genesys Symbols -->
<ff-success></ff-success>
<ff-advantage></ff-advantage>
<ff-triumph></ff-triumph>
<ff-failure></ff-failure>
<ff-threat></ff-threat>
<ff-despair></ff-despair>
```

One thing to note: it's only important to specify the system if it's `eote`. Since `genesys` is assumed to be the 
default, we onl look for `eote` on the shortcut.
