// ---------------------------------------------------------------------------------------------------------------------
// Fantasy Flight Web Components
// ---------------------------------------------------------------------------------------------------------------------

import { buildFFDiceSymbolWrapper, FFDiceSymbol } from './components/ffDiceSymbols';

// ---------------------------------------------------------------------------------------------------------------------
// Low Level
// ---------------------------------------------------------------------------------------------------------------------

customElements.define('ff-symbol', FFDiceSymbol);

// ---------------------------------------------------------------------------------------------------------------------
// Dice Wrappers
// ---------------------------------------------------------------------------------------------------------------------

for(const symbol of [ 'proficiency', 'ability', 'boost', 'force', 'challenge', 'difficulty', 'setback' ])
{
    customElements.define(`ff-${ symbol }`, buildFFDiceSymbolWrapper(symbol));
}

// ---------------------------------------------------------------------------------------------------------------------
// Symbol Wrappers
// ---------------------------------------------------------------------------------------------------------------------

// Positive Symbols
for(const symbol of [ 'success', 'advantage', 'triumph', 'lightside', 'forcepoint' ])
{
    customElements.define(`ff-${ symbol }`, buildFFDiceSymbolWrapper(symbol));
    customElements.define(`ff-${ symbol }-eote`, buildFFDiceSymbolWrapper(symbol, 'eote'));
}

// Negative Symbols
for(const symbol of [ 'failure', 'threat', 'despair', 'darkside' ])
{
    customElements.define(`ff-${ symbol }`, buildFFDiceSymbolWrapper(symbol));
    customElements.define(`ff-${ symbol }-eote`, buildFFDiceSymbolWrapper(symbol, 'eote'));
}

// ---------------------------------------------------------------------------------------------------------------------
