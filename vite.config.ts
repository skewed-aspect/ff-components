//----------------------------------------------------------------------------------------------------------------------
// Vite Config
//----------------------------------------------------------------------------------------------------------------------

import path from 'path';
import { defineConfig } from 'vite';

//----------------------------------------------------------------------------------------------------------------------

export default defineConfig({
    root: 'src/demo',
    publicDir: 'assets',
    server: {
        port: 4200,
    },
    resolve: {
        alias: [
            {
                find: /~(.+)/,
                replacement: path.join(process.cwd(), 'node_modules/$1')
            },
        ]
    },
    build: {
        lib: {
            entry: path.resolve(__dirname, 'src/ff-components.ts'),
            name: 'FFWebComponents',
            fileName: (format) => `ff-components.${format}.js`
        },
        outDir: '../../dist/',
        emptyOutDir: true,
        cssCodeSplit: true,
    }
});

//----------------------------------------------------------------------------------------------------------------------
